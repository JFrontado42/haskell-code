module Arbol where
data Arbol = Hoja Int | Nodo Int Arbol Arbol

minelem :: Arbol Int -> Int
minelem (Hoja x y)= Hoja

valida :: Arbol Int -> Bool
valida (Hoja _ _) = True
valida (Hoja x _)| ((minelem Hoja <= minelem x) and valida x) = True | otherwise = False
valida (Hoja _ y)| minelem Hoja <= minelem y and valida y = True | otherwise = False
valida (Hoja x y)| minelem Hoja <= minelem x and minelem Hoja <= minelem y and valida x and valida y = True | otherwise = False