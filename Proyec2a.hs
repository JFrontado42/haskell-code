palin :: (Eq a) => [a] -> Bool
palin [] = error "la columna no puede estar vacia"
palin n = n == (reverse n)

patron :: [a] -> Int -> [[a]]
patron [] i = error "no puede estar vacia"
patron y i = if(i <= 0) then error"El entero tiene que ser mayor a 0" else rotar (concat (replicate i y)) (length y)

rota :: [a] -> Int -> [a]
rota l n = drop n l  ++ take n l

rotar :: [a] -> Int -> [[a]]
rotar l n = [ rota l i | i <- [0 .. (n) -1]]
