data Arbol a = Hoja Int | Nodo Int (Arbol a) (Arbol a)

nodor :: Arbol a -> [Int]
nodor (Nodo n a b) = [n] ++ nodor a ++ nodor b
nodor a = []

hojar :: Arbol a -> [Int]
hojar (Hoja x) = [x]
hojar (Nodo n a b) = hojar a ++ hojar b

heap :: Arbol a -> Bool
heap (a)| ((maximum (nodor a) <= minimum (hojar a)) && list_ord (nodor a)) =True | otherwise =False

list_ord::Ord a=>[a]->Bool
list_ord [] = True
list_ord [_] = True
list_ord (x:y:xs) = (x<=y) && list_ord (y:xs)