
import System.IO
import Data.List

describeList :: [a] -> String
describeList xs = case xs of [] -> "una lista"
[x] -> "una lista" 
xs -> "una"



patron :: [a] -> Int -> [[a]]
patron y i = rotar (concat (replicate i y)) (length y)

rota :: [a] -> Int -> [a]
rota l n = drop n l  ++ take n l

rotar :: [a] -> Int -> [[a]]
rotar l n = [ rota l i | i <- [0 .. (n) -1]]

main = do

handle <- openFile "fun.in" ReadMode
contents <- hGetContents handle
let hola = lines contents
let palabras = map words hola
print palabras 
hClose handle


