--Jesus Frontado
--Victor Capua
import Data.List (transpose)-- para usar transpose
data Arbol a = Hoja Int | Nodo Int (Arbol a) (Arbol a)

--evalua si es palindrome

palin :: (Eq a) => [a] -> Bool
palin n = n == reverse n

--replica la palabra i veces las concatena y las comienza a rotar
--para formar la palabra en patron vertical solo hay que rotarla 
--por el numero de elementos que la conforman love 4 veces y asi 

patron :: [a] -> Int -> [[a]]
patron y i = rotar (concat (replicate i y)) (length y)

--la rotacion tomo un elemento y se lo sumo a otro

rota :: [a] -> Int -> [a]
rota l n = drop n l  ++ take n l

-- para formar el patron

rotar :: [a] -> Int -> [[a]]
rotar l n = [ rota l i | i <- [0 .. (n) -1]]

-- tomo una lista de lista las traspongo, las filas las convierto --en columnas y viceversa a cada fila de la matriz le aplico la --comprobacion y eevaluo los resultados con and 

esPalHor :: (Eq a) => [[a]] -> Bool
esPalHor n =  and (map palin (transpose n))

-- recorre las hojas de izq a der
nodor :: Arbol a -> [Int]
nodor (Nodo n a b) = [n] ++ nodor a ++ nodor b 
nodor a = []

-- recorre las hojas de izq a der

hojar :: Arbol a -> [Int] 
hojar (Hoja x) = [x]
hojar (Nodo n a b) = hojar a ++ hojar b

--principio de transitividad si a>b y b>c then a>c 

heap :: Arbol a -> Bool
heap (a)| ((maximum (nodor a) <= minimum (hojar a)) && list_ord (nodor a)) =True | otherwise =False

list_ord::Ord a=>[a]->Bool -- comprueba si la lista esta en list_ord [] = True -- orden nodor trae los nodos 
list_ord [_] = True
list_ord (x:y:xs) = (x<=y) && list_ord (y:xs)

